INSERT INTO users VALUES (1, 'krunal', 'krunal@gmail.com');
INSERT INTO users VALUES (2, 'alex', 'alex@yahoo.com');
INSERT INTO users VALUES (3, 'joel', 'joel@gmail.com');



INSERT INTO product_type VALUES (product_type_seq.nextval, 'Standard',true);
INSERT INTO product_type VALUES (product_type_seq.nextval, 'Deluxe',false);
INSERT INTO product_type VALUES (product_type_seq.nextval, 'Platinum',true);


INSERT INTO product VALUES ( product_seq.nextval,'Managed Server', 1, true);
INSERT INTO product VALUES (product_seq.nextval,'Utility Storage',1,false);
INSERT INTO product VALUES (product_seq.nextval,'Fiber Optic Cable',2,true);

INSERT INTO product_price VALUES (product_price_seq.nextval,1,1,true,2500.00);
INSERT INTO product_price VALUES (product_price_seq.nextval,1,2, true,3200.00);
INSERT INTO product_price VALUES (product_price_seq.nextval, 2,1,false,5000.00);