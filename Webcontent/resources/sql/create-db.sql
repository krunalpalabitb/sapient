DROP TABLE users IF EXISTS;

CREATE TABLE users (
  id         INTEGER PRIMARY KEY,
  name VARCHAR(30),
  email  VARCHAR(50)
);

drop table product_type if exists;

drop sequence product_type_seq if exists;
create sequence product_type_seq;

create table product_type(
	product_type_id bigint  primary key,
	name varchar(255),
	is_active boolean
);

drop sequence product_seq if exists;
create sequence product_seq;

Drop table product if exists;

create table product (
    product_id bigint  primary key,
    name varchar(255),
    product_type_id Integer,
    FOREIGN KEY (product_type_id) 
    REFERENCES  product_type(product_type_id),
    is_active boolean
    

);

drop table product_price if exists;

drop sequence product_price_seq if exists;
create sequence product_price_seq;


create table product_price(

	product_price_id bigint  primary key,
	product_type_id Integer,
	product_id  Integer,
	FOREIGN KEY (product_type_id) 
    REFERENCES  product_type(product_type_id),
    FOREIGN KEY (product_id) 
    REFERENCES  product(product_id),
	is_active boolean,
	price decimal(10,2)
	
	
);
