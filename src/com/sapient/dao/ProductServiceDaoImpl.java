package com.sapient.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sapient.valueobject.Product;
import com.sapient.valueobject.ProductPrice;
import com.sapient.valueobject.ProductType;
import com.sapient.wrapper.ProductWrapper;

@Repository
public class ProductServiceDaoImpl {

	@Autowired
	private SessionFactory factory;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Product> getAllProducts() {

		Session session = factory.openSession();
		List<Product> productList = null;
		List list = session.createQuery("FROM Product P ").list();

		Iterator it = list.iterator();
		
		while (it.hasNext()) {
			
			Object obj = it.next();
			Product product = (Product) obj;
			product.getProductType();
			product.getProductPrices();

		}

		productList = list;

		return productList;

	}

	public void addProduct(Map<String, Object> map) {
		// TODO Auto-generated method stub

		Session session = factory.getCurrentSession();

		Product product = (Product) map.get("Product");
		ProductType productType = (ProductType) map.get("ProductType");
		ProductPrice productprce = (ProductPrice) map.get("ProductPrice");

		session.save(productType);

		product.setProductType(productType);
		productprce.setProductType(productType);
		productprce.setProduct(product);

		session.save(product);

		session.save(productprce);

	}

	@SuppressWarnings("rawtypes")
	public void deleteProduct(ProductWrapper prodWrapper) {

		Session session = factory.getCurrentSession();
		String hql = "FROM Product P WHERE P.name = :name";

		Query query = session.createQuery(hql);
		query.setParameter("name", prodWrapper.getName());
		
		List results = query.list();
		Product product = null;
		
		if (results != null && results.size() > 0) {
			
			Iterator it = results.iterator();
			
			while (it.hasNext()) {
				
				Object obj = it.next();
				product = (Product) obj;
				break;
			}
		}
		
		for (ProductPrice p : product.getProductPrices()) {
			session.delete(p);
		}

		session.delete(product.getProductType());

		session.delete(product);

	}

}
