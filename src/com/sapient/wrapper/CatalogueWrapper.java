package com.sapient.wrapper;

import java.util.List;

public class CatalogueWrapper {
	
	List<ProductWrapper> productList;

	public List<ProductWrapper> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductWrapper> productList) {
		this.productList = productList;
	}
	
	

}
