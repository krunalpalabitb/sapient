package com.sapient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.service.ProductServiceImpl;
import com.sapient.service.ValidationServiceImpl;
import com.sapient.wrapper.CatalogueWrapper;
import com.sapient.wrapper.OperationResult;
import com.sapient.wrapper.ProductWrapper;


@RequestMapping("/productServices")
@RestController
public class ProductController {
	
	@Autowired
	private ProductServiceImpl productService;
	
	@Autowired
	private ValidationServiceImpl validationService;

	
	@RequestMapping(value ="/displayProducts" ,produces="application/json")
	public @ResponseBody CatalogueWrapper getAllProducts(){
		
		
		return productService.getAllProducts();
	}
	
	
	
	@RequestMapping(value ="/addProduct" ,produces="application/json",
			method = RequestMethod.POST, consumes="application/json")
	public @ResponseBody OperationResult addProduct(@RequestBody ProductWrapper prodWrapper){
		
		OperationResult or = new OperationResult();
		validationService.validateProduct(prodWrapper,or);
		if(or.isSuccess()){
			productService.addProduct(prodWrapper);
		}
		
		return or;
	}
	
	
	@RequestMapping(value ="/deleteProduct" ,produces="application/json",
			method = RequestMethod.POST, consumes="application/json")
	public @ResponseBody OperationResult deleteProduct(@RequestBody ProductWrapper prodWrapper){
		
		OperationResult or = new OperationResult();
		validationService.validateProduct(prodWrapper,or);
		if(or.isSuccess()){
			productService.deleteProduct(prodWrapper);
		}
		
		return or;
	}
	

}

