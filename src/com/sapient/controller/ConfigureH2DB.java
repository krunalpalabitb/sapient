package com.sapient.controller;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class ConfigureH2DB {
	
	@Bean(name = "myDataSource")
	public DataSource dataSource() {
		
		System.out.println("Here");;

		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.H2).addScript("../../resources/sql/create-db.sql").addScript("../../resources/sql/insert-data.sql").build();
		return db;

	}
	

}
