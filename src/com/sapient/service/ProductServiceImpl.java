package com.sapient.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sapient.dao.ProductServiceDaoImpl;
import com.sapient.valueobject.Product;
import com.sapient.valueobject.ProductPrice;
import com.sapient.valueobject.ProductType;
import com.sapient.wrapper.CatalogueWrapper;
import com.sapient.wrapper.ProductWrapper;

@Service
public class ProductServiceImpl {
	
	@Autowired
	private ProductServiceDaoImpl productDao;
	
	@Transactional(readOnly=true)
	public CatalogueWrapper getAllProducts(){
		
		List<Product> productList = productDao.getAllProducts();
		
		CatalogueWrapper cw = new CatalogueWrapper();
		List<ProductWrapper> toSend = new ArrayList<ProductWrapper>();
		
		ProductWrapper productWrper = null;
		for(Product p: productList){
			
			productWrper = new ProductWrapper();
			productWrper.setIsActive(p.getIsActive());
			productWrper.setName(p.getName());
			productWrper.setProductType(p.getProductType().getName());
			
			for(ProductPrice pPrice : p.getProductPrices()){
				if(pPrice.getProductType().getName().equals(p.getProductType().getName())){
					productWrper.setPrice(pPrice.getPrice());
					break;
				}
			}
				
			toSend.add(productWrper);
		}
		cw.setProductList(toSend);
		
		return cw;
		
	}

	@Transactional(isolation=Isolation.SERIALIZABLE,propagation=Propagation.REQUIRES_NEW)
	public void addProduct(ProductWrapper prodWrapper) {
		
		Product pToAdd = new Product();
		ProductType pType = new ProductType();
		
		ProductPrice pPrice = new ProductPrice();
		pPrice.setIsActive(true);
		pPrice.setPrice(prodWrapper.getPrice());
		
		pToAdd.setName(prodWrapper.getName());
		pToAdd.setIsActive(true);
		
		pType.setIsActive(true);
		pType.setName(prodWrapper.getProductType());
		
		Map<String, Object> objMap = new HashMap<String, Object>();
		objMap.put("Product",pToAdd);
		objMap.put("ProductType",pType);
		objMap.put("ProductPrice",pPrice);
		
		productDao.addProduct(objMap);
		
	}

	@Transactional(isolation=Isolation.SERIALIZABLE,propagation=Propagation.REQUIRES_NEW)
	public void deleteProduct(ProductWrapper prodWrapper) {
		
		productDao.deleteProduct(prodWrapper);
		
	}

}
